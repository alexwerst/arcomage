import {playerComponents} from "./constants/player-components";
import {enemyComponents} from "./constants/enemy-components";

export default class Resources {
    constructor(player, enemy) {
        this.setResources(player, playerComponents);
        this.setResources(enemy, enemyComponents);
    }

    setResources(obj, components) {
        for (let i in obj) {
            if (obj.hasOwnProperty(i)) {
                switch (i) {
                    case 'tower':
                        components.TOWER_COUNT.innerHTML = obj[i];
                        components.TOWER.style.height = obj[i] * 3 + 'px';
                        break;
                    case 'wall':
                        components.WALL_COUNT.innerHTML = obj[i];
                        components.WALL.style.height = obj[i] * 3 + 'px';
                        break;
                    case 'magic':
                        components.MAGIC.innerHTML = obj[i];
                        break;
                    case 'gems':
                        components.GEMS.innerHTML = obj[i];
                        break;
                    case 'quarry':
                        components.QUARRY.innerHTML = obj[i];
                        break;
                    case 'bricks':
                        components.BRICKS.innerHTML = obj[i];
                        break;
                    case 'dungeon':
                        components.DUNGEON.innerHTML = obj[i];
                        break;
                    case 'recruits':
                        components.RECRUITS.innerHTML = obj[i];
                        break;
                    default:
                        return true;
                }
            }
        }
    }
}