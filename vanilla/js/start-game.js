import CardsDisplay from "./cards-display";
import Resources from "./set-resources";
import allCards from "./constants/cards";
import CardActions from "./card-actions";

export default class StartGame {
    constructor(player, enemy) {
        this.player = player;
        this.enemy = enemy;
        this._init();
    }

    _init() {
        const actions = new CardActions(allCards);

        if (!localStorage.startGame) {
            localStorage.gameDeck = JSON.stringify(actions.shuffleCards());
            actions.giveOutCards(JSON.parse(localStorage.gameDeck));
            localStorage.playerOneResources = JSON.stringify(this.player);
            localStorage.playerTwoResources = JSON.stringify(this.enemy);
            localStorage.gameTurn = '1';
        }

        new CardsDisplay();

        new Resources(JSON.parse(localStorage.playerOneResources), JSON.parse(localStorage.playerTwoResources));
    }
}