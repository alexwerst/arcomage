import CardActions from "./card-actions";
import {playerComponents} from "./constants/player-components";
import {enemyComponents} from "./constants/enemy-components";

export default class CardsDisplay {
    constructor() {
        this._init();
    }

    _init() {
        if (localStorage.startGame) {
            const actions = new CardActions();

            if (localStorage.gameTurn === '1') {
                enemyComponents.CARDS.classList.add('hidden');
                playerComponents.CARDS.classList.remove('hidden');
            } else if (localStorage.gameTurn === '2') {
                enemyComponents.CARDS.classList.remove('hidden');
                playerComponents.CARDS.classList.add('hidden');
            }

            actions.showPlayerCards(JSON.parse(localStorage.playerOneCards), JSON.parse(localStorage.playerOneResources), playerComponents.CARDS);
            actions.showPlayerCards(JSON.parse(localStorage.playerTwoCards), JSON.parse(localStorage.playerTwoResources), enemyComponents.CARDS);
        }
    }


}
