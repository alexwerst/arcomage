/*
    Condition:
        if ( @COMPARATOR1.@OBJECT1 > @COMPARATOR2.@OBJECT2 )
            result: first - true
                    second - false
*/

const allCards = [
    {
        id: 1,
        imgUrl: 'card_1',
        action: {
            player: {
                bricks: -8
            },
            enemy: {
                bricks: -8
            }
        },
        price: {
            bricks: 0
        }
    },
    {
        id: 2,
        imgUrl: 'card_2',
        action: {
            player: {
                bricks: 2,
                gems: 2
            }
        },
        playAgain: true,
        price: {
            bricks: 0
        }
    },
    {
        id: 3,
        imgUrl: 'card_3',
        action: {
            player: {
                wall: 1,
                bricks: -1
            }
        },
        playAgain: true,
        price: {
            bricks: 1
        }
    },
    {
        id: 4,
        imgUrl: 'card_4',
        action: {
            player: {
                quarry: 1,
                bricks: -3
            }
        },
        price: {
            bricks: 3
        }
    },
    {
        id: 5,
        imgUrl: 'card_5',
        condition: {
            comparator1: 'player',
            object1: 'quarry',
            comparator2: 'enemy',
            object2: 'quarry',
            result: {
                first: {
                    player: {
                        quarry: 1,
                        bricks: -4
                    }
                },
                second: {
                    player: {
                        quarry: 2,
                        bricks: -4
                    }
                }
            }
        },
        price: {
            bricks: 4
        }
    },
    {
        id: 6,
        imgUrl: 'card_6',
        action: {
            player: {
                wall: 4,
                quarry: 1,
                bricks: -7
            }
        },
        price: {
            bricks: 7
        }
    },
    {
        id: 7,
        imgUrl: 'card_7',
        action: {
            player: {
                wall: 5,
                gems: -6,
                bricks: -2
            }
        },
        price: {
            bricks: 2
        }
    },
    {
        id: 9,
        imgUrl: 'card_9',
        action: {
            player: {
                wall: 3,
                bricks: -2
            }
        },
        price: {
            bricks: 2
        }
    },
    {
        id: 10,
        imgUrl: 'card_10',
        action: {
            player: {
                wall: 4,
                bricks: -3
            }
        },
        price: {
            bricks: 3
        }
    },
    {
        id: 11,
        imgUrl: 'card_11',
        action: {
            player: {
                gems: 4,
                quarry: 1,
                bricks: -2
            },
            enemy: {
                quarry: 1
            }
        },
        price: {
            bricks: 2
        }
    },
    {
        id: 12,
        imgUrl: 'card_12',
        condition: {
            comparator1: 'player',
            object1: 'wall',
            comparator2: '0',
            result: {
                first: {
                    wall: 3,
                    bricks: -3
                },
                second: {
                    wall: 6,
                    bricks: -3
                }
            }
        },
        price: {
            bricks: 3
        }
    },
    {
        id: 13,
        imgUrl: 'card_13',
        action: {
            player: {
                wall: -5,
                bricks: -7
            },
            enemy: {
                wall: -5
            }
        },
        playAgain: true,
        price: {
            bricks: 7
        }
    },
    {
        id: 14,
        imgUrl: 'card_14',
        action: {
            player: {
                magic: 1,
                bricks: -8
            }
        },
        playAgain: true,
        price: {
            bricks: 8
        }
    },
    {
        id: 15,
        imgUrl: 'card_15',
        action: {
            player: {
                quarry: -1
            },
            enemy: {
                quarry: -1
            }
        },
        price: {
            bricks: 0
        }
    },
    {
        id: 16,
        imgUrl: 'card_16',
        action: {
            player: {
                wall: 6,
                bricks: -5
            }
        },
        price: {
            bricks: 5
        }
    },
    {
        id: 17,
        imgUrl: 'card_17',
        action: {
            player: {
                bricks: -4
            },
            enemy: {
                quarry: -1
            }
        },
        price: {
            bricks: 4
        }
    },
    {
        id: 18,
        imgUrl: 'card_18',
        action: {
            player: {
                quarry: 2,
                bricks: -6
            }
        },
        price: {
            bricks: 6
        }
    },
    {
        id: 19,
        imgUrl: 'card_19',
        action: {
            player: {
                quarry: -1,
                wall: 10,
                gems: 5
            }
        },
        price: {
            bricks: 0
        }
    },
    {
        id: 20,
        imgUrl: 'card_20',
        action: {
            player: {
                wall: 8,
                bricks: -8
            }
        },
        price: {
            bricks: 8
        }
    },
    {
        id: 21,
        imgUrl: 'card_21',
        action: {
            player: {
                wall: 5,
                dungeon: 1,
                bricks: -9
            }
        },
        price: {
            bricks: 9
        }
    },
    {
        id: 22,
        imgUrl: 'card_22',
        action: {
            player: {
                wall: 7,
                gems: 7,
                bricks: -9
            }
        },
        price: {
            bricks: 9
        }
    },
    {
        id: 23,
        imgUrl: 'card_23',
        action: {
            player: {
                wall: 6,
                tower: 3,
                bricks: -11
            }
        },
        price: {
            bricks: 11
        }
    },
    {
        id: 24,
        imgUrl: 'card_24',
        action: {
            player: {
                wall: 12,
                bricks: -13
            }
        },
        price: {
            bricks: 13
        }
    },
    {
        id: 25,
        imgUrl: 'card_25',
        action: {
            player: {
                wall: 8,
                tower: 5,
                bricks: -15
            }
        },
        price: {
            bricks: 15
        }
    },
    {
        id: 26,
        imgUrl: 'card_26',
        action: {
            player: {
                wall: 15,
                bricks: -16
            }
        },
        price: {
            bricks: 16
        }
    },
    {
        id: 27,
        imgUrl: 'card_27',
        action: {
            player: {
                wall: 6,
                bricks: -18
            },
            enemy: {
                damage: 10
            }
        },
        price: {
            bricks: 18
        }
    },
    {
        id: 28,
        imgUrl: 'card_28',
        action: {
            player: {
                wall: 20,
                tower: 8,
                bricks: -24
            }
        },
        price: {
            bricks: 24
        }
    },
    {
        id: 29,
        imgUrl: 'card_29',
        action: {
            player: {
                wall: 9,
                recruits: -5,
                bricks: -7
            }
        },
        price: {
            bricks: 7
        }
    },
    {
        id: 30,
        imgUrl: 'card_30',
        action: {
            player: {
                wall: 1,
                tower: 1,
                recruits: 2,
                bricks: -1
            }
        },
        price: {
            bricks: 1
        }
    },
    {
        id: 32,
        imgUrl: 'card_32',
        condition: {
            comparator1: 'player',
            object1: 'dungeon',
            comparator2: 'enemy',
            object2: 'dungeon',
            result: {
                first: {
                    player: {
                        recruits: 6,
                        wall: 6,
                        bricks: -10
                    }
                },
                second: {
                    player: {
                        recruits: 6,
                        wall: 6,
                        bricks: -10,
                        dungeon: 1
                    }
                }
            }
        },
        price: {
            bricks: 10
        }
    },
    {
        id: 33,
        imgUrl: 'card_33',
        action: {
            player: {
                wall: 7,
                bricks: -14
            },
            enemy: {
                damage: 6
            }
        },
        price: {
            bricks: 14
        }
    },
    {
        id: 35,
        imgUrl: 'card_35',
        action: {
            player: {
                tower: 1,
                gems: -1
            }
        },
        playAgain: true,
        price: {
            gems: 1
        }
    },
    {
        id: 36,
        imgUrl: 'card_36',
        action: {
            player: {
                gems: -2
            },
            enemy: {
                tower: -1
            }
        },
        playAgain: true,
        price: {
            gems: 2
        }
    },
    {
        id: 37,
        imgUrl: 'card_37',
        action: {
            player: {
                tower: 3,
                gems: -2
            }
        },
        price: {
            gems: 2
        }
    },
    {
        id: 38,
        imgUrl: 'card_38',
        action: {
            player: {
                magic: 1,
                gems: -3
            }
        },
        price: {
            gems: 3
        }
    },
    {
        id: 40,
        imgUrl: 'card_40',
        action: {
            player: {
                tower: 5,
                gems: -5
            }
        },
        price: {
            gems: 5
        }
    },
    {
        id: 41,
        imgUrl: 'card_41',
        action: {
            player: {
                tower: 2,
                gems: -4
            },
            enemy: {
                tower: -2
            }
        },
        price: {
            gems: 4
        }
    },
    {
        id: 42,
        imgUrl: 'card_42',
        action: {
            player: {
                tower: 3,
                magic: 1,
                gems: -6
            },
            enemy: {
                tower: 1
            }
        },
        price: {
            gems: 6
        }
    },
    {
        id: 43,
        imgUrl: 'card_43',
        action: {
            player: {
                gems: -2
            },
            enemy: {
                tower: -3
            }
        },
        price: {
            gems: 2
        }
    },
    {
        id: 44,
        imgUrl: 'card_44',
        action: {
            player: {
                tower: 5,
                gems: -3
            }
        },
        price: {
            gems: 3
        }
    },
    {
        id: 45,
        imgUrl: 'card_45',
        action: {
            player: {
                gems: -4
            },
            enemy: {
                tower: -5
            }
        },
        price: {
            gems: 4
        }
    },
    {
        id: 46,
        imgUrl: 'card_46',
        action: {
            player: {
                tower: -5,
                magic: 2,
                gems: -3
            }
        },
        price: {
            gems: 3
        }
    },
    {
        id: 47,
        imgUrl: 'card_47',
        action: {
            player: {
                magic: 1,
                wall: 3,
                tower: 3,
                gems: -7
            }
        },
        price: {
            gems: 7
        }
    },
    {
        id: 49,
        imgUrl: 'card_49',
        action: {
            player: {
                tower: 8,
                gems: -6
            }
        },
        price: {
            gems: 6
        }
    },
    {
        id: 50,
        imgUrl: 'card_50',
        action: {
            player: {
                magic: 1,
                tower: 5,
                gems: -9
            }
        },
        price: {
            gems: 9
        }
    },
    {
        id: 51,
        imgUrl: 'card_51',
        action: {
            player: {
                magic: -1,
                gems: -8
            },
            enemy: {
                tower: -9
            }
        },
        price: {
            gems: 8
        }
    },
    {
        id: 52,
        imgUrl: 'card_52',
        action: {
            player: {
                tower: 5,
                gems: -7
            },
            enemy: {
                bricks: -6
            }
        },
        price: {
            gems: 7
        }
    },
    {
        id: 53,
        imgUrl: 'card_53',
        action: {
            player: {
                tower: 11,
                gems: -10
            }
        },
        price: {
            gems: 10
        }
    },
    {
        id: 54,
        imgUrl: 'card_54',
        action: {
            player: {
                tower: -7,
                magic: -1,
                gems: -5
            },
            enemy: {
                tower: -7,
                magic: -1
            }
        },
        price: {
            gems: 5
        }
    },
    {
        id: 55,
        imgUrl: 'card_55',
        condition: {
            comparator1: 'player',
            object1: 'tower',
            comparator2: 'enemy',
            object2: 'tower',
            result: {
                first: {
                    player: {
                        tower: 1
                    }
                },
                second: {
                    player: {
                        tower: 2
                    }
                }
            }
        },
        price: {
            gems: 0
        }
    },
    {
        id: 56,
        imgUrl: 'card_56',
        action: {
            player: {
                tower: 11,
                wall: -6,
                gems: -8
            }
        },
        price: {
            gems: 8
        }
    },
    {
        id: 57,
        imgUrl: 'card_57',
        action: {
            player: {
                tower: 20,
                gems: -21
            }
        },
        price: {
            gems: 21
        }
    },
    {
        id: 58,
        imgUrl: 'card_58',
        action: {
            player: {
                tower: 12,
                gems: -17
            },
            enemy: {
                damage: 6
            }
        },
        price: {
            gems: 17
        }
    },
    {
        id: 59,
        imgUrl: 'card_59',
        action: {
            player: {
                tower: 10,
                wall: 5,
                recruits: 5,
                gems: -15
            }
        },
        price: {
            gems: 15
        }
    },
    {
        id: 60,
        imgUrl: 'card_60',
        action: {
            player: {
                tower: 15,
                gems: -16
            }
        },
        price: {
            gems: 16
        }
    },
    {
        id: 61,
        imgUrl: 'card_61',
        action: {
            player: {
                tower: 8,
                dungeon: 1,
                gems: -14
            }
        },
        price: {
            gems: 14
        }
    },
    {
        id: 62,
        imgUrl: 'card_62',
        action: {
            player: {
                tower: 8,
                wall: 3,
                gems: -12
            }
        },
        price: {
            gems: 12
        }
    },
    {
        id: 63,
        imgUrl: 'card_63',
        action: {
            player: {
                tower: 7,
                bricks: -10,
                gems: -4
            }
        },
        price: {
            gems: 4
        }
    },
    {
        id: 64,
        imgUrl: 'card_64',
        action: {
            player: {
                tower: 6,
                gems: -13
            },
            enemy: {
                tower: -4
            }
        },
        price: {
            gems: 13
        }
    },
    {
        id: 65,
        imgUrl: 'card_65',
        action: {
            player: {
                tower: 1,
                gems: 3
            },
            enemy: {
                tower: 1
            }
        },
        playAgain: true,
        price: {
            gems: 0
        }
    },
    {
        id: 66,
        imgUrl: 'card_66',
        action: {
            player: {
                tower: 4,
                recruits: -3,
                gems: -5
            },
            enemy: {
                tower: -2
            }
        },
        price: {
            gems: 5
        }
    },
    {
        id: 67,
        imgUrl: 'card_67',
        condition: {
            comparator1: 'player',
            object1: 'tower',
            comparator2: 'enemy',
            object2: 'wall',
            result: {
                first: {
                    player: {
                        gems: -11
                    },
                    enemy: {
                        tower: -8
                    }
                },
                second: {
                    player: {
                        gems: -11
                    },
                    enemy: {
                        damage: 8
                    }
                }
            }
        },
        price: {
            gems: 11
        }
    },
    {
        id: 68,
        imgUrl: 'card_68',
        action: {
            player: {
                tower: 13,
                recruits: 6,
                bricks: 6,
                gems: -18
            }
        },
        price: {
            gems: 18
        }
    },
    {
        id: 69,
        imgUrl: 'card_69',
        action: {
            player: {
                recruits: -6
            },
            enemy: {
                recruits: -6
            }
        },
        price: {
            recruits: 0
        }
    },
    {
        id: 70,
        imgUrl: 'card_70',
        action: {
            player: {
                recruits: -1
            },
            enemy: {
                damage: 2
            }
        },
        playAgain: true,
        price: {
            recruits: 1
        }
    },
    {
        id: 71,
        imgUrl: 'card_71',
        action: {
            player: {
                gems: -3,
                recruits: -1
            },
            enemy: {
                damage: 4
            }
        },
        price: {
            recruits: 1
        }
    },
    {
        id: 72,
        imgUrl: 'card_72',
        action: {
            player: {
                dungeon: 1,
                recruits: -3
            }
        },
        price: {
            recruits: 3
        }
    },
    {
        id: 74,
        imgUrl: 'card_74',
        action: {
            player: {
                damage: 3,
                recruits: -3
            },
            enemy: {
                damage: 6
            }
        },
        price: {
            recruits: 3
        }
    },
    {
        id: 75,
        imgUrl: 'card_75',
        action: {
            player: {
                damage: 1,
                recruits: -4
            },
            enemy: {
                tower: -3
            }
        },
        price: {
            recruits: 4
        }
    },
    {
        id: 76,
        imgUrl: 'card_76',
        action: {
            player: {
                recruits: -6
            },
            enemy: {
                tower: -2
            }
        },
        playAgain: true,
        price: {
            recruits: 6
        }
    },
    {
        id: 77,
        imgUrl: 'card_77',
        action: {
            player: {
                recruits: -3
            },
            enemy: {
                damage: 5
            }
        },
        price: {
            recruits: 3
        }
    },
    {
        id: 78,
        imgUrl: 'card_78',
        action: {
            player: {
                wall: 3,
                recruits: -5
            },
            enemy: {
                damage: 4
            }
        },
        price: {
            recruits: 5
        }
    },
    {
        id: 79,
        imgUrl: 'card_79',
        action: {
            player: {
                recruits: -6
            },
            enemy: {
                tower: -4
            }
        },
        price: {
            recruits: 6
        }
    },
    {
        id: 80,
        imgUrl: 'card_80',
        action: {
            player: {
                dungeon: 2,
                recruits: -7
            }
        },
        price: {
            recruits: 7
        }
    },
    {
        id: 81,
        imgUrl: 'card_81',
        action: {
            player: {
                tower: 2,
                wall: 4,
                recruits: -8
            },
            enemy: {
                damage: 2
            }
        },
        price: {
            recruits: 8
        }
    },
    {
        id: 82,
        imgUrl: 'card_82',
        action: {
            player: {
                dungeon: 1,
                recruits: 3
            },
            enemy: {
                dungeon: 1
            }
        },
        price: {
            recruits: 0
        }
    },
    {
        id: 83,
        imgUrl: 'card_83',
        action: {
            player: {
                recruits: -5
            },
            enemy: {
                damage: 6
            }
        },
        price: {
            recruits: 5
        }
    },
    {
        id: 84,
        imgUrl: 'card_84',
        action: {
            player: {
                recruits: -6
            },
            enemy: {
                damage: 7
            }
        },
        price: {
            recruits: 6
        }
    },
    {
        id: 85,
        imgUrl: 'card_85',
        action: {
            player: {
                recruits: -6
            },
            enemy: {
                recruits: -3,
                damage: 6
            }
        },
        price: {
            recruits: 6
        }
    },
    {
        id: 86,
        imgUrl: 'card_86',
        action: {
            player: {
                bricks: -5,
                gems: -5,
                recruits: -10
            },
            enemy: {
                bricks: -5,
                gems: -5,
                recruits: -5,
                damage: 6
            }
        },
        price: {
            recruits: 5
        }
    },
    {
        id: 87,
        imgUrl: 'card_87',
        condition: {
            comparator1: 'enemy',
            object1: 'wall',
            comparator2: '0',
            result: {
                first: {
                    player: {
                        recruits: -8
                    },
                    enemy: {
                        damage: 6
                    }
                },
                second: {
                    player: {
                        recruits: -8
                    },
                    enemy: {
                        damage: 10
                    }
                }
            }
        },
        price: {
            recruits: 8
        }
    },
    {
        id: 88,
        imgUrl: 'card_88',
        action: {
            player: {
                recruits: -9
            },
            enemy: {
                damage: 9
            }
        },
        price: {
            recruits: 9
        }
    },
    {
        id: 89,
        imgUrl: 'card_89',
        condition: {
            comparator1: 'enemy',
            object1: 'wall',
            comparator2: '0',
            result: {
                first: {
                    player: {
                        recruits: -11
                    },
                    enemy: {
                        damage: 10
                    }
                },
                second: {
                    player: {
                        recruits: -11
                    },
                    enemy: {
                        damage: 7
                    }
                }
            }
        },
        price: {
            recruits: 11
        }
    },
    {
        id: 90,
        imgUrl: 'card_90',
        condition: {
            comparator1: 'player',
            object1: 'magic',
            comparator2: 'enemy',
            object2: 'magic',
            result: {
                first: {
                    player: {
                        recruits: -9
                    },
                    enemy: {
                        damage: 12
                    }
                },
                second: {
                    player: {
                        recruits: -9
                    },
                    enemy: {
                        damage: 8
                    }
                }
            }
        },
        price: {
            recruits: 9
        }
    },
    {
        id: 91,
        imgUrl: 'card_91',
        condition: {
            comparator1: 'player',
            object1: 'wall',
            comparator2: 'enemy',
            object2: 'wall',
            result: {
                first: {
                    player: {
                        recruits: -10
                    },
                    enemy: {
                        tower: -6
                    }
                },
                second: {
                    player: {
                        recruits: -10
                    },
                    enemy: {
                        damage: 6
                    }
                }
            }
        },
        price: {
            recruits: 10
        }
    },
    {
        id: 92,
        imgUrl: 'card_92',
        action: {
            player: {
                recruits: -14
            },
            enemy: {
                tower: -5,
                recruits: -8
            }
        },
        price: {
            recruits: 14
        }
    },
    {
        id: 93,
        imgUrl: 'card_93',
        action: {
            player: {
                recruits: -11
            },
            enemy: {
                damage: 8,
                quarry: -1
            }
        },
        price: {
            recruits: 11
        }
    },
    {
        id: 94,
        imgUrl: 'card_94',
        action: {
            player: {
                gems: 5,
                bricks: 2,
                recruits: -12
            },
            enemy: {
                gems: -10,
                bricks: -5
            }
        },
        price: {
            recruits: 12
        }
    },
    {
        id: 95,
        imgUrl: 'card_95',
        action: {
            player: {
                wall: 4,
                recruits: -15
            },
            enemy: {
                damage: 10
            }
        },
        price: {
            recruits: 15
        }
    },
    {
        id: 96,
        imgUrl: 'card_96',
        action: {
            player: {
                recruits: -17
            },
            enemy: {
                damage: 10,
                dungeon: -1,
                recruits: -5
            }
        },
        price: {
            recruits: 17
        }
    },
    {
        id: 97,
        imgUrl: 'card_97',
        action: {
            player: {
                recruits: -25
            },
            enemy: {
                damage: 20,
                dungeon: -1,
                gems: -10
            }
        },
        price: {
            recruits: 25
        }
    },
    {
        id: 98,
        imgUrl: 'card_98',
        condition: {
            comparator1: 'player',
            object1: 'wall',
            comparator2: 'enemy',
            object2: 'wall',
            result: {
                first: {
                    player: {
                        recruits: -2
                    },
                    enemy: {
                        damage: 3
                    }
                },
                second: {
                    player: {
                        recruits: -2
                    },
                    enemy: {
                        damage: 2
                    }
                }
            }
        },
        price: {
            recruits: 2
        }
    },
    {
        id: 99,
        imgUrl: 'card_99',
        action: {
            player: {
                gems: 1,
                recruits: -2
            },
            enemy: {
                damage: 3
            }
        },
        price: {
            recruits: 2
        }
    },
    {
        id: 100,
        imgUrl: 'card_100',
        action: {
            player: {
                tower: -3,
                recruits: -4
            },
            enemy: {
                damage: 8
            }
        },
        price: {
            recruits: 4
        }
    },
    {
        id: 101,
        imgUrl: 'card_101',
        action: {
            player: {
                gems: -3,
                recruits: -13
            },
            enemy: {
                damage: 13
            }
        },
        price: {
            recruits: 13
        }
    },
    {
        id: 102,
        imgUrl: 'card_102',
        action: {
            player: {
                recruits: -18
            },
            enemy: {
                tower: -12
            }
        },
        price: {
            recruits: 18
        }
    }
];

export default allCards;