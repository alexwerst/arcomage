export const playerComponents = {
    CARDS: document.getElementById('pl-cards'),

    QUARRY: document.getElementById('pl-quarry'),
    BRICKS: document.getElementById('pl-bricks'),

    MAGIC: document.getElementById('pl-magic'),
    GEMS: document.getElementById('pl-gems'),

    DUNGEON: document.getElementById('pl-dungeon'),
    RECRUITS: document.getElementById('pl-recruits'),

    TOWER: document.getElementById('pl-tower'),
    TOWER_COUNT: document.getElementById('pl-t-count'),
    WALL: document.getElementById('pl-wall'),
    WALL_COUNT: document.getElementById('pl-w-count'),
};