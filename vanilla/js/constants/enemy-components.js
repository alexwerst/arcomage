export const enemyComponents = {
    CARDS: document.getElementById('enemy-cards'),

    QUARRY: document.getElementById('enemy-quarry'),
    BRICKS: document.getElementById('enemy-bricks'),

    MAGIC: document.getElementById('enemy-magic'),
    GEMS: document.getElementById('enemy-gems'),

    DUNGEON: document.getElementById('enemy-dungeon'),
    RECRUITS: document.getElementById('enemy-recruits'),

    TOWER: document.getElementById('enemy-tower'),
    TOWER_COUNT: document.getElementById('enemy-t-count'),
    WALL: document.getElementById('enemy-wall'),
    WALL_COUNT: document.getElementById('enemy-w-count'),
};