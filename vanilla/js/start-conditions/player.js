let Player = {
    tower: 15,
    wall: 25,
    magic: 2,
    gems: 5,
    quarry: 4,
    bricks: 7,
    dungeon: 2,
    recruits: 5
};

export default Player;