export default class CardDescription {
    constructor(obj, turn) {
        this.turn = turn;
        this._init(obj);
    }

    _init(obj) {

        if (this.turn === 'player') {
            this.playerRes = JSON.parse(localStorage.playerOneResources);
            this.enemyRes = JSON.parse(localStorage.playerTwoResources);
        } else if (this.turn === 'enemy') {
            this.playerRes = JSON.parse(localStorage.playerTwoResources);
            this.enemyRes = JSON.parse(localStorage.playerOneResources);
        }

        for (let i in obj) {
            if (i === 'action') {
                this._mainAction(obj.action);
            } else if (i === 'condition') {
                let compOne = obj.condition.comparator1,
                  objOne = obj.condition.object1,
                  compTwo = obj.condition.comparator2,
                  objTwo = obj.condition.object2,
                  comparator,
                  result;

                switch (compOne) {
                    case 'player':
                        compOne = this.playerRes;
                        break;
                    case 'enemy':
                        compOne = this.enemyRes;
                        break;
                    default:
                        compOne = this.playerRes;
                }

                switch (compTwo) {
                    case 'player':
                        compTwo = this.playerRes;
                        break;
                    case 'enemy':
                        compTwo = this.enemyRes;
                        break;
                    case '0':
                        compTwo = 0;
                        break;
                    default:
                        compTwo = this.enemyRes;
                }

                if (compTwo && objTwo) {
                    comparator = compTwo[objTwo];
                } else {
                    comparator = 0;
                }

                if (compOne[objOne] > comparator) {
                    result = obj.condition.result.first;
                } else {
                    result = obj.condition.result.second;
                }

                this._mainAction(result);

            } else if (i === 'playAgain') {
                localStorage.playAgain = 'true';
            } else if (!obj.hasOwnProperty('playAgain')) {
                delete localStorage.playAgain
            }
        }
    }

    _mainAction(obj) {
        for (let i in obj) {
            if (i === 'player') {

                for (let i in obj.player) {
                    this.playerRes[i] += obj.player[i];
                    if (this.playerRes[i] < 0 && i !== 'tower') {
                        this.playerRes[i] = 0;
                    }

                    if (i === 'damage') {
                        let damage = obj.player.damage;
                        this._damageAction(damage, this.playerRes);
                    }
                }

                if (this.turn === 'player') {
                    localStorage.playerOneResources = JSON.stringify(this.playerRes);
                } else if (this.turn === 'enemy') {
                    localStorage.playerTwoResources = JSON.stringify(this.playerRes);
                }
            }
            if (i === 'enemy') {

                for (let i in obj.enemy) {
                    this.enemyRes[i] += obj.enemy[i];
                    if (this.enemyRes[i] < 0 && i !== 'tower') {
                        this.enemyRes[i] = 0;
                    }

                    if (i === 'damage') {
                        let damage = obj.enemy.damage;
                        this._damageAction(damage, this.enemyRes);
                    }
                }

                if (this.turn === 'player') {
                    localStorage.playerTwoResources = JSON.stringify(this.enemyRes);
                } else if (this.turn === 'enemy') {
                    localStorage.playerOneResources = JSON.stringify(this.enemyRes);
                }
            }
        }
    }

    _damageAction(damage, resources) {
        if (resources.wall - damage > 0) {
            resources.wall -= damage;
        } else if (resources.wall - damage < 0) {
            resources.tower -= damage - resources.wall;
            resources.wall = 0;
        } else if (resources.wall = 0) {
            resources.tower -= damage;
        }
    }
}