import StartGame from './start-game';
import Player from './start-conditions/player';
import Enemy from './start-conditions/enemy';
import PlayerActions from './player-actions';


new StartGame(Player, Enemy);

new PlayerActions();
