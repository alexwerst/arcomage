import CardsDisplay from "./cards-display";
import {playerComponents} from "./constants/player-components";
import {enemyComponents} from "./constants/enemy-components";
import CardDescription from "./card-description";
import Resources from "./set-resources";

export default class PlayerActions {
    constructor() {
        this._onCardClick();
    }

    _onCardClick() {
        let gameDeck = JSON.parse(localStorage.gameDeck);

        document.addEventListener('click', (e) => {
            if (e.target.classList.contains('card-enabled')) {

                let cards = this._getCardDeck(e.target.parentNode);

                let card_id = e.target.dataset.item;

                for (let i = 0; i < cards.length; i++) {
                    if (cards[i].id === +card_id) {

                        if (e.target.parentNode === playerComponents.CARDS) {
                            new CardDescription(cards[i], 'player');
                        } else if (e.target.parentNode === enemyComponents.CARDS) {
                            new CardDescription(cards[i], 'enemy');
                        }

                        gameDeck.push(cards[i]);
                        cards.splice(i, 1);

                        cards.push(gameDeck.shift());

                        localStorage.gameDeck = JSON.stringify(gameDeck);

                        if (e.target.parentNode === playerComponents.CARDS) {
                            localStorage.playerOneCards = JSON.stringify(cards);
                        } else if (e.target.parentNode === enemyComponents.CARDS) {
                            localStorage.playerTwoCards = JSON.stringify(cards);
                        }
                    }
                }

                if (localStorage.gameTurn === '1' && !localStorage.playAgain) {
                    localStorage.gameTurn = '2';
                } else if (localStorage.gameTurn === '2' && !localStorage.playAgain) {
                    localStorage.gameTurn = '1';

                    this._updateResources();
                }
            }

            this._updateGameArena();

            setTimeout(() => {
                this._winnerCheck();
            })

        });

        document.addEventListener('contextmenu', (e) => {
            e.preventDefault();

            if (e.target.classList.contains('card')) {

                let cards = this._getCardDeck(e.target.parentNode);

                let card_id = e.target.dataset.item;

                for (let i = 0; i < cards.length; i++) {
                    if (cards[i].id === +card_id) {

                        gameDeck.push(cards[i]);
                        cards.splice(i, 1);

                        cards.push(gameDeck.shift());

                        localStorage.gameDeck = JSON.stringify(gameDeck);

                        if (e.target.parentNode === playerComponents.CARDS) {
                            localStorage.playerOneCards = JSON.stringify(cards);
                        } else if (e.target.parentNode === enemyComponents.CARDS) {
                            localStorage.playerTwoCards = JSON.stringify(cards);
                        }
                    }
                }

                if (localStorage.gameTurn === '1') {
                    localStorage.gameTurn = '2';
                } else if (localStorage.gameTurn === '2') {
                    localStorage.gameTurn = '1';

                    this._updateResources();
                }
            }

            this._updateGameArena();

        });
    }

    _getCardDeck(target) {
        if (target === playerComponents.CARDS) {
            return JSON.parse(localStorage.playerOneCards);
        } else if (target === enemyComponents.CARDS) {
            return JSON.parse(localStorage.playerTwoCards);
        }
    }

    _updateResources() {
        let playerOneResources = JSON.parse(localStorage.playerOneResources),
          playerTwoResources = JSON.parse(localStorage.playerTwoResources);

        playerOneResources.bricks += playerOneResources.quarry;
        playerOneResources.gems += playerOneResources.magic;
        playerOneResources.recruits += playerOneResources.dungeon;

        playerTwoResources.bricks += playerTwoResources.quarry;
        playerTwoResources.gems += playerTwoResources.magic;
        playerTwoResources.recruits += playerTwoResources.dungeon;

        localStorage.playerOneResources = JSON.stringify(playerOneResources);
        localStorage.playerTwoResources = JSON.stringify(playerTwoResources);
    }

    _updateGameArena() {
        new CardsDisplay();
        new Resources(JSON.parse(localStorage.playerOneResources), JSON.parse(localStorage.playerTwoResources));
    }

    _winnerCheck() {
        const playerOneTower = JSON.parse(localStorage.playerOneResources).tower,
            playerTwoTower = JSON.parse(localStorage.playerTwoResources).tower;

        if (playerOneTower <= 0) {
            alert('Player #2 win!');
        } else if (playerTwoTower <= 0) {
            alert('Player #1 win!')
        }

        if (playerOneTower >= 100) {
            alert('Player #1 win!');
        } else if (playerTwoTower >= 100) {
            alert('Player #2 win!')
        }

    }
}