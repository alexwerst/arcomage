export default class CardActions {
    constructor(cards) {
        this.cards = cards;
    }

    shuffleCards() {
        return this.shuffle(this.cards.slice(0));
    }

    shuffle(array) {
        let currentIndex = array.length, temporaryValue, randomIndex;

        while (0 !== currentIndex) {

            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    giveOutCards(arr) {
        let playerCards = [],
          enemyCards = [];

        for (let i = 0; i < 10; i++) {
            i % 2 === 0 ? playerCards.push(arr[i]) : enemyCards.push(arr[i]);
        }

        arr.splice(0, 10);

        localStorage.playerOneCards = JSON.stringify(playerCards);
        localStorage.playerTwoCards = JSON.stringify(enemyCards);
        localStorage.gameDeck = JSON.stringify(arr);
        localStorage.startGame = 'true';
    }

    showPlayerCards(arr, arr2, element) {

        element.innerHTML = '';

        for (let i = 0; i < arr.length; i++) {
            let item = document.createElement('div');

            item.classList.add('card');
            item.dataset.item = arr[i].id;
            item.style.background = 'url("./cards/' + arr[i].imgUrl + '.jpg")';
            element.appendChild(item);

            for (let j in arr[i].price) {
                arr[i].price[j] <= arr2[j] ? item.classList.add('card-enabled') : item.classList.add('card-disabled');
            }
        }
    }

}