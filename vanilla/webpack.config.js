const path = require('path');

module.exports = {
    mode: 'development',

    entry: './js/app.js',

    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'public/js')
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2015']
                    }
                }
            }
        ]
    },

    devServer: {
        contentBase: path.join(__dirname, 'public'),
        compress: true,
        port: 5555
    },

    devtool: 'cheap-eval-source-map'
};